#!/usr/bin/env python
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
"""TBD."""

__author__ = "Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>"

import logging
import os
import unittest

from ART.art_header import ArtHeader
from ART.art_testcase import ArtTestCase
from ART.art_misc import set_log_level


class TestArtHeader(ArtTestCase):
    """TBD."""

    def __init__(self, *args, **kwargs):
        """Init."""
        super(TestArtHeader, self).__init__(*args, **kwargs)
        self.cwd = os.getcwd()

    def setUp(self):
        """Setup log level."""
        set_log_level(logging.WARN)
        os.chdir(self.cwd)

    def test_already_set(self):
        """Test for already set header."""
        header = ArtHeader(os.path.join(self.test_directory, "data_art_header/Validate/test/test_AlreadySet.sh"))
        self.assertEqual(header.validate(), False)
        self.assertEqual(header.get("art-description"), 'first')

    def test_duplicate_invalid_key(self):
        """Test for duplicate invalid header."""
        header = ArtHeader(os.path.join(self.test_directory, "data_art_header/Validate/test/test_DuplicateInvalidKey.sh"))
        self.assertEqual(header.validate(), False)
        self.assertEqual(header.get("art-invalid-key"), 'value')

    def test_integer_value(self):
        """Test for integer type header."""
        header = ArtHeader(os.path.join(self.test_directory, "data_art_header/Validate/test/test_IntegerValue.sh"))
        self.assertEqual(header.validate(), True)
        self.assertEqual(header.get("art-input-nfiles"), 14)

    def test_invalid_key(self):
        """Test for invalid header."""
        header = ArtHeader(os.path.join(self.test_directory, "data_art_header/Validate/test/test_InvalidKey.sh"))
        self.assertEqual(header.validate(), False)
        self.assertEqual(header.get("art-invalid-key"), 'value')

    def test_invalid_type(self):
        """Test for invalid type header."""
        header = ArtHeader(os.path.join(self.test_directory, "data_art_header/Validate/test/test_InvalidType.sh"))
        self.assertEqual(header.validate(), False)
        self.assertEqual(header.get("art-cores"), 'NotANumber')

    def test_invalid_value(self):
        """Test for invalid value header."""
        header = ArtHeader(os.path.join(self.test_directory, "data_art_header/Validate/test/test_InvalidValue.sh"))
        self.assertEqual(header.validate(), False)
        self.assertEqual(header.get("art-type"), 'invalid-value')

    def test_missing_type(self):
        """Test for missing type header."""
        header = ArtHeader(os.path.join(self.test_directory, "data_art_header/Validate/test/test_MissingType.sh"))
        self.assertEqual(header.validate(), True)
        self.assertEqual(header.get("art-type"), None)

    def test_no_value(self):
        """Test for no value header."""
        header = ArtHeader(os.path.join(self.test_directory, "data_art_header/Validate/test/test_NoValue.sh"))
        self.assertEqual(header.validate(), True)
        self.assertEqual(header.get("art-no-value-test"), None)

    def test_not_executable(self):
        """Test for non executable script."""
        header = ArtHeader(os.path.join(self.test_directory, "data_art_header/Validate/test/test_NotExecutable.sh"))
        self.assertEqual(header.validate(), False)
        self.assertEqual(header.get("art-type"), 'grid')

    def test_undefined(self):
        """Test for undefined header."""
        header = ArtHeader(os.path.join(self.test_directory, "data_art_header/Validate/test/test_Undefined.sh"))
        self.assertEqual(header.validate(), True)
        self.assertEqual(header.get("art-undefined"), None)

    def test_wrong_format_1(self):
        """Test for wrong format 1."""
        header = ArtHeader(os.path.join(self.test_directory, "data_art_header/Validate/test/test_WrongFormat1.sh"))
        self.assertEqual(header.validate(), False)
        self.assertEqual(header.get("art-format1"), None)

    def test_wrong_format_2(self):
        """Test for wrong format 2."""
        header = ArtHeader(os.path.join(self.test_directory, "data_art_header/Validate/test/test_WrongFormat2.sh"))
        self.assertEqual(header.validate(), False)
        self.assertEqual(header.get("art-format2"), None)

    def test_wrong_format_3(self):
        """Test for wrong format 3."""
        header = ArtHeader(os.path.join(self.test_directory, "data_art_header/Validate/test/test_WrongFormat3.sh"))
        self.assertEqual(header.validate(), False)
        self.assertEqual(header.get("art-format3"), None)


if __name__ == '__main__':
    unittest.main()
