# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
export AtlasBuildBranch=21.0
export AtlasProject=Athena
export Athena_PLATFORM=x86_64-slc6-gcc62-opt
export AtlasBuildStamp=latest
