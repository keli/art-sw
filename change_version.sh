#!/bin/sh
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
find scripts | xargs grep -slZ 1.0.23 | xargs sed -i '' 's/1\.0\.23/1\.0\.24/g'
