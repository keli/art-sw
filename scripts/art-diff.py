#!/usr/bin/env python
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
"""
ART  - ATLAS Release Tester - Diff.

Usage:
  art-diff.py [--diff-type=<diff_type> --file=<pattern>... --exclude=<pattern>... --platform-ref=<platform> --entries=<entries> --mode=<mode> --order-trees] <nightly_release_ref> <project_ref> <nightly_tag_ref> <package>
  art-diff.py [--diff-type=<diff_type> --file=<pattern>... --exclude=<pattern>... --entries=<entries> --mode=<mode> --order-trees --keep] <path> <ref_path>

Options:
  --diff-type=<diff_type>    Type of diff (e.g. diff-pool, diff-root, diff-txt) [default: diff-pool]
  --entries=<entries>        Only diff over number of entries [default: -1]
  --exclude=<pattern>...     Exclude test files according to pattern
  --file=<pattern>...        Compare the following txt files (diff-txt) or patterns (diff-root) which default to *AOD*.pool.root *ESD*.pool.root *HITS*.pool.root *RDO*.pool.root *TAG*.root
  -h --help                  Show this screen
  --keep                     Keep tmp dirs for debug
  --mode=<mode>              Sets the mode for diff-root {summary, detailed} [default: detailed]
  --order-trees              Order trees for diff-root
  --platform-ref=<platform>  Reference Platform [default: x86_64-slc6-gcc62-opt]
  --test-name=<test_name>    Test name to compare
  --version                  Show version

Arguments:
  path                       Directory or File to compare
  nightly_release_ref        Reference Name of the nightly release (e.g. 21.0)
  nightly_tag_ref            Reference Nightly tag (e.g. 2017-02-26T2119)
  package                    Package of the test (e.g. Tier0ChainTests)
  project_ref                Reference Name of the project (e.g. Athena)
  ref_path                   Directory or File to compare to

Environment:
  AtlasBuildBranch           Name of the nightly release (e.g. 21.0)
  AtlasProject               Name of the project (e.g. Athena)
  <AtlasProject>_PLATFORM    Platform (e.g. x86_64-slc6-gcc62-opt)
  AtlasBuildStamp            Nightly tag (e.g. 2017-02-26T2119)
"""

__author__ = "Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>"
__version__ = "1.0.24"

import atexit
import fnmatch
import glob
import os
import re
import shlex
import shutil
import subprocess
import sys
import tarfile
import tempfile

from ART.docopt import docopt

ATHENA_STDOUT = "athena_stdout.txt"
DEFAULT_ENTRIES = -1
DEFAULT_MODE = "detailed"


class ArtDiff(object):
    """Class for comparing output files."""

    EOS_OUTPUT_DIR = '/eos/atlas/atlascerngroupdisk/data-art/grid-output'

    def __init__(self):
        """Constructor of ArtDiff."""
        self.default_file_patterns = ['*AOD*.pool.root', '*ESD*.pool.root', '*HITS*.pool.root', '*RDO*.pool.root', '*TAG*.root']

    def parse(self, arguments):
        """Called from comandline."""
        diff_type = arguments['--diff-type']
        files = self.default_file_patterns if diff_type == 'diff-pool' else list(set(arguments['--file']))
        entries = arguments['--entries']
        mode = arguments['--mode']
        excludes = arguments['--exclude']
        order_trees = arguments['--order-trees']
        self.keep = arguments['--keep']
        if arguments['<nightly_release_ref>'] is not None:  # pragma: no cover
            try:
                nightly_release = os.environ['AtlasBuildBranch']
                project = os.environ['AtlasProject']
                platform = os.environ[project + '_PLATFORM']
                nightly_tag = os.environ['AtlasBuildStamp']
            except KeyError, e:
                print "Environment variable not set", e
                sys.exit(-1)

            nightly_release_ref = arguments['<nightly_release_ref>']
            project_ref = arguments['<project_ref>']
            platform_ref = arguments['--platform-ref']
            nightly_tag_ref = arguments['<nightly_tag_ref>']

            package = arguments['<package>']
            print nightly_release, project, platform, nightly_tag, nightly_release_ref, project_ref, platform_ref, nightly_tag_ref
            exit(self.diff(nightly_release, project, platform, nightly_tag, nightly_release_ref, project_ref, platform_ref, nightly_tag_ref, package, diff_type, files, excludes, entries=entries, mode=mode, order_trees=order_trees))

        if diff_type == 'diff-txt' and not files:
            print "Error: --file should be used if option --diff-type=diff-txt"
            sys.exit(-1)

        # if there are tar files, untar
        path = self._check_for_tar(arguments['<path>'], 'val-')
        ref_path = self._check_for_tar(arguments['<ref_path>'], 'ref-')

        if os.path.isfile(path):
            # file compare
            if not os.path.isfile(ref_path):
                print "Error: <ref_path> should be a file, if <path> is a file."
                print "  <path>     ", path
                print "  <ref_path> ", ref_path
                sys.exit(-1)

            exit(self.diff_file(path, ref_path, diff_type, entries=entries, mode=mode, order_trees=order_trees))

        if os.path.isfile(ref_path):
            print "Error: <ref_path> should be a directory, if <path> is a directory."
            print "  <path>     ", path
            print "  <ref_path> ", ref_path
            sys.exit(-1)

        # check if path contains "test_" directories
        if len([o for o in glob.glob(os.path.join(path, 'test_*')) if os.path.isdir(o)]) > 0:
            # directory compare
            result = self.diff_dirs(path, ref_path, diff_type, files, excludes, entries=entries, mode=mode, order_trees=order_trees)
            if result < 0:
                print "Error: No files found to compare"
                print "  <path>     ", path
                print "  <ref_path> ", ref_path
            sys.exit(result)

        # single test compare
        result = self.diff_test(path, ref_path, diff_type, files, entries=entries, mode=mode, order_trees=order_trees)
        if result < 0:
            print "Error: No files found to compare"
            print "  <path>     ", path
            print "  <ref_path> ", ref_path
        sys.exit(result)

    def _check_for_tar(self, path, prefix):
        """Check for tar file and untar."""
        file = None if os.path.isdir(path) else os.path.basename(path)
        path = path if os.path.isdir(path) else os.path.dirname(path)
        tars = glob.glob(os.path.join(path, 'user.*.tar'))
        if len(tars) > 0:
            tmp = tempfile.mkdtemp(prefix=prefix)
            if not self.keep:
                atexit.register(shutil.rmtree, tmp, ignore_errors=True)
            for tar_path in tars:
                tar = tarfile.open(tar_path)
                for member in tar.getmembers():
                    # does not work: tar.extractall()
                    tar.extract(member, path=tmp)
            path = tmp
        return os.path.join(path, file) if file else path

    def diff(self, nightly_release, project, platform, nightly_tag, nightly_release_ref, project_ref, platform_ref, nightly_tag_ref, package, diff_type, files, excludes=[], entries=DEFAULT_ENTRIES, mode=DEFAULT_MODE, order_trees=False):  # pragma: no cover
        """Run difference between two results."""
        path = os.path.join(ArtDiff.EOS_OUTPUT_DIR, nightly_release, project, platform, nightly_tag, package)
        ref_path = os.path.join(ArtDiff.EOS_OUTPUT_DIR, nightly_release_ref, project_ref, platform_ref, nightly_tag_ref, package)
        return self.diff_dirs(path, ref_path, diff_type, files, excludes, entries=entries, mode=mode, order_trees=order_trees)

    def diff_dirs(self, path, ref_path, diff_type, files, excludes=[], entries=DEFAULT_ENTRIES, mode=DEFAULT_MODE, order_trees=False):
        """Run difference between two directories."""
        print "diff_dirs()"
        print "    path: %s" % path
        print "ref_path: %s" % ref_path

        if not os.path.exists(ref_path):
            print "Ref dir not found", ref_path
            return -1

        stat_per_chain = {}
        for test_name in [o for o in glob.glob(os.path.join(path, 'test_*')) if os.path.isdir(o)]:
            test_name = os.path.basename(test_name)

            # skip tests in pattern
            exclude_test = False
            for exclude in excludes:
                if fnmatch.fnmatch(test_name, exclude):
                    print "Excluding test %s according to pattern %s" % (test_name, exclude)
                    exclude_test = True
                    break
            if exclude_test:
                continue

            print "******************************************"
            print "Test: %s" % test_name
            print "******************************************"
            stat_per_chain[test_name] = self.diff_test(os.path.join(path, test_name), os.path.join(ref_path, test_name), diff_type, files, entries=entries, mode=mode, order_trees=order_trees)

        # if no tests found, exit code is -1
        result = 0 if stat_per_chain else -1
        for test_name, status in stat_per_chain.iteritems():
            result |= status
            if status > 0:
                print "%-70s CHANGED" % test_name
            elif status == 0:
                print "%-70s IDENTICAL" % test_name

        return result

    def diff_test(self, test, ref_test, diff_type, files, entries=DEFAULT_ENTRIES, mode=DEFAULT_MODE, order_trees=False):
        """Run differences between two directories."""
        # the tar files my be in the test subdir
        test = self._check_for_tar(test, 'val-')
        ref_test = self._check_for_tar(ref_test, 'ref-')

        print "diff_test()"
        print "    test: %s" % test
        print "ref_test: %s" % ref_test

        # optional printout of stdout
        if diff_type != "diff-txt":
            result = self.get_result(test)
            ref_result = self.get_result(ref_test)
            for key, value in result.iteritems():
                if key in ref_result:
                    print "%-10s: ref: %d events, val: %d events" % (key, int(ref_result[key][1]), int(result[key][1]))

        if os.path.isdir(test):
            test_dir = test
            # get files in all patterns
            test_files = []
            for test_pattern in files:
                test_files.extend(glob.glob(os.path.join(test_dir, test_pattern)))
        else:
            test_files = [os.path.basename(test)]

        # run test over all files, no files => -1
        result = 0 if test_files else -1
        for test_file in test_files:
            basename = os.path.basename(test_file)
            val_file = os.path.join(test, basename)
            ref_file = os.path.join(ref_test, basename)
            print "val_file: %s" % val_file
            print "ref_file: %s" % ref_file

            result |= self.diff_file(val_file, ref_file, diff_type, entries=entries, mode=mode, order_trees=order_trees)

        return result

    def diff_file(self, path, ref_path, diff_type, entries=DEFAULT_ENTRIES, mode=DEFAULT_MODE, order_trees=False):
        """Compare two files."""
        print "diff_file()"
        if not os.path.exists(ref_path):
            print "no test found in ref_dir to compare: %s" % ref_path
            return -1

        if fnmatch.fnmatch(path, '*TAG*.root'):
            return self.diff_tag(path, ref_path)

        if diff_type == 'diff-pool':
            return self.diff_pool(path, ref_path)

        if diff_type == 'diff-txt':
            return self.diff_txt(path, ref_path)

        return self.diff_root(path, ref_path, entries, mode, order_trees)

    def get_result(self, directory):
        """
        Return map [ESD|AOD,...] -> (success, succeeded event count).

        find event counts in logfile
        'Event count check for AOD to TAG passed: all processed events found (500 output events)'
        'Event count check for BS to ESD failed: found 480 events, expected 500'
        """
        result = {}
        if os.path.isdir(directory):
            for entry in os.listdir(directory):
                if re.match(r"tarball_PandaJob_(\d+)_(\w+)", entry):
                    logfile = os.path.join(directory, entry, ATHENA_STDOUT)
                    with open(logfile, "r") as f:
                        for line in f:
                            match = re.search(r"Event count check for \w+ to (\w+) (passed|failed):[^\d]+(\d+)", line)
                            if match:
                                result[match.group(1)] = (match.group(2), match.group(3))
        return result

    def diff_txt(self, file_name, ref_file):
        """TBD."""
        (code, out, err) = self.run_command("diff -a " + file_name + " " + ref_file)
        if code != 0:  # pragma: no cover
            print "Error: %d" % code
            print err

        print out
        return code

    def diff_tag(self, file_name, ref_file):
        """TBD."""
        (code, out, err) = self.run_command("diffTAGTree.py " + file_name + " " + ref_file)
        if code != 0:  # pragma: no cover
            print "Error: %d" % code
            print err

        print out
        return code

    def diff_pool(self, file_name, ref_file):
        """TBD."""
        import PyUtils.PoolFile as PF

        # diff-pool
        df = PF.DiffFiles(refFileName=ref_file, chkFileName=file_name, ignoreList=['RecoTimingObj_p1_RAWtoESD_timings', 'RecoTimingObj_p1_ESDtoAOD_timings'])
        df.printSummary()
        stat = df.status()
        print stat
        del df

        return stat

    def diff_root(self, file_name, ref_file, entries, mode, order_trees):
        """TBD."""
        # diff-root
        (code, out, err) = self.run_command("acmd.py diff-root " + ref_file + " " + file_name + " --error-mode resilient --ignore-leaves RecoTimingObj_p1_HITStoRDO_timings RecoTimingObj_p1_RAWtoESD_mems RecoTimingObj_p1_RAWtoESD_timings RAWtoESD_mems RAWtoESD_timings ESDtoAOD_mems ESDtoAOD_timings HITStoRDO_timings RAWtoALL_mems RAWtoALL_timings RecoTimingObj_p1_RAWtoALL_mems RecoTimingObj_p1_RAWtoALL_timings RecoTimingObj_p1_EVNTtoHITS_timings EVNTtoHITS_timings RecoTimingObj_p1_Bkg_HITStoRDO_timings index_ref" + (" --order-trees" if order_trees else "") + " --entries " + str(entries) + " --mode " + mode)
        if code != 0:  # pragma: no cover
            print "Error: %d" % code
            print err

        print out
        return code

    def run_command(self, cmd, dir=None, shell=False, env=None):
        """
        Run the given command locally.

        The command runs as separate subprocesses for every piped command.
        Returns tuple of exit_code, output and err.
        """
        print "Execute:", cmd
        if "|" in cmd:  # pragma: no cover
            cmd_parts = cmd.split('|')
        else:
            cmd_parts = []
            cmd_parts.append(cmd)
        i = 0
        p = {}
        for cmd_part in cmd_parts:
            cmd_part = cmd_part.strip()
            if i == 0:
                p[i] = subprocess.Popen(shlex.split(cmd_part), stdin=None, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=dir, shell=shell, env=env)
            else:  # pragma: no cover
                p[i] = subprocess.Popen(shlex.split(cmd_part), stdin=p[i - 1].stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=dir, shell=shell, env=env)
            i = i + 1
        (output, err) = p[i - 1].communicate()
        exit_code = p[0].wait()

        return exit_code, str(output), str(err)


if __name__ == '__main__':  # pragma: no cover
    if sys.version_info < (2, 7, 0):
        sys.stderr.write("You need python 2.7 or later to run this script\n")
        exit(1)

    arguments = docopt(__doc__, version=os.path.splitext(os.path.basename(__file__))[0] + ' ' + __version__)
    ArtDiff().parse(arguments)
