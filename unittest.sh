#!/bin/sh
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
source test/atlas_env.sh
source share/localSetupART.sh
source test/localTestSetup.sh

mkdir -p tmp-unit

export PYTHONPATH=${SCRIPTPATH}/../test:${PYTHONPATH}

exit_code=0
for test_name in grid; do
# for test_name in base build configuration diff grid header misc rucio script trigger xrdcp; do
  (cd tmp-unit && python -m unittest $* test_art_${test_name})
  exit_code=${exit_code} || $?
done

echo ${exit_code}
exit ${exit_code}
