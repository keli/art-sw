<?php
// Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
// Author: Tulay Cuhadar Donszelmann (tcuhadar@cern.ch)

// Configuration
// $script_to_dir = '../path';  // path from script to top-level browsing dir
                             // (e.g. '../browse-here' if tar2fs.php resides
                             // in a parallel directory to browse-here)
$script_to_dir = '../grid-output/test';  // path from script to top-level browsing dir
                            // (e.g. '../browse-here' if tar2fs.php resides
                            // in a parallel directory to browse-here)


// Server version is PHP 5.3.3,
// - no array dereferencing of functions (function()[0] -> use temp var)
// - no array declarations with squera brackets ([1,2] -> array(1, 2))

function _header($header) {
  global $debug;

  if ($debug) {
    print "HEADER($header)\n";
  } else {
    header($header);
  }
}

function tempdir($dir = null, $prefix = 'tmp_', $mode = 0700, $maxAttempts = 1000)
{
  /* Use the system temp dir by default. */
  if (is_null($dir)) {
      $dir = sys_get_temp_dir();
  }

  /* Trim trailing slashes from $dir. */
  $dir = rtrim($dir, DIRECTORY_SEPARATOR);

  /* If we don't have permission to create a directory, fail, otherwise we will
   * be stuck in an endless loop.
   */
  if (!is_dir($dir) || !is_writable($dir)) {
    return false;
  }

  /* Make sure characters in prefix are safe. */
  if (strpbrk($prefix, '\\/:*?"<>|') !== false) {
    return false;
  }

  /* Attempt to create a random directory until it works. Abort if we reach
   * $maxAttempts.
   */
  $attempts = 0;
  do {
    $path = sprintf('%s%s%s%s', $dir, DIRECTORY_SEPARATOR, $prefix, mt_rand(100000, mt_getrandmax()));
  } while (
    !mkdir($path, $mode) &&
    $attempts++ < $maxAttempts
  );

  return $path;
}

function startsWith($haystack, $needle) {
  if (strlen($needle) == 0) return true;
  if (strlen($needle) > strlen($haystack)) return false;
  return substr_compare($haystack, $needle, 0, strlen($needle)) === 0;
}

function endsWith($haystack, $needle) {
  return substr_compare($haystack, $needle, -strlen($needle)) === 0;
}

function formatBytes($size, $precision = 0) {
  if ($size <= 0) {
    return '0';
  }
  $base = log($size, 1024);
  $suffixes = array('', 'K', 'M', 'G', 'T', 'P');

  return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
}

function get_filename($fname, $level) {
  $parts = explode('/', $fname, $level + 1);
  if (count($parts) > $level) {
    $fname = $parts[$level];
  }
  return $fname;
}

function get_levelname($list, $level) {
  if ($level == 0) return '';
  foreach ($list as $entry) {
    list($fname, $mtime, $fsize, $isdir) = $entry;
    $parts = explode('/', $fname, $level + 1);
    if (count($parts) > $level) {
      $parts = array_slice($parts, 0, $level);
      return implode('/', $parts).'/';
    }
  }
  return '';
}

function dir_list($path) {
  $result = scandir($path);
  $output = array();
  if ($result) {
    foreach ($result as $fname) {
      $full_path = $path.'/'.$fname;
      $mtime = filemtime($full_path);
      $fsize = filesize($full_path);
      $isdir = is_dir($full_path);
      $islink = is_link($full_path);

      if (endsWith($fname, '.log.tar')) {
        $fname = 'tarball_logs/';
        $fsize = 0;
        $isdir = true;
      } else if (endsWith($fname, '.tar')) {
        $output = array_merge($output, tar_list($full_path));
        continue;
      }

      array_push($output, array($fname, $mtime, $fsize, $isdir, $islink));
    }
  }
  return $output;
}

function get_index($rpath) {
  if (!is_dir($rpath)) return $rpath;
  $index = $rpath.'/index.html';
  if (is_file($index)) return $index;
  $index = $rpath.'/index.php';
  if (is_file($index)) return $index;

  return $rpath;
}


function tar_decode($line) {
  // Unix format (6):  -rw-r--r-- tcuhadar/staff       10 2019-05-02 09:56 path/to/files/b.txt
  // Unix format (6):  drwxr-xr-x tcuhadar/staff       10 2019-05-02 09:56 path/to/files/
  // Unix format (8):  lrwxrwxrwx tcuhadar/staff       10 2019-05-02 09:56 path/to/files/link.txt -> b.txt
  // Mac  format (9):  -rw-r--r--  0 tcuhadar   staff      262 Feb 14  2018 path/to/files/b.txt
  // Mac  format (9):  drwxr-xr-x  0 tcuhadar   staff        0 Apr  9 07:25 path/to/files/
  // Mac  format (11): drwxr-xr-x  0 tcuhadar   staff        0 Apr  9 07:25 path/to/files/link.txt -> b.txt
  $entry = preg_split('/ +/', $line);
  if (count($entry) >= 9) {
    // Mac format
    $size_index = 4;
    $hour_index = 7;
    $link_index = 10;

    $fname = $entry[8];
    if (strpos($entry[$hour_index], ':')) {
      $year = date("Y");
      $timestamp = "$entry[6]-$entry[5]-$year $entry[7]";
    } else {
      $timestamp = "$entry[6]-$entry[5]-$entry[7]";
    }
    $islink = count($entry) == 11;
  } else {
    // Unix format
    $size_index = 2;
    $link_index = 7;

    $fname = $entry[5];
    $timestamp = "$entry[3] $entry[4]";
    $islink = count($entry) == 8;
  }
  $fname .= $islink && endsWith($entry[$link_index], '/') ? '/' : '';
  $mtime = strtotime($timestamp);
  $size = intval($entry[$size_index]);
  $isdir = endsWith($fname, '/');
  return array($fname, $mtime, $size, $isdir, $islink);
}

function tar_list($tar) {
  $options = endsWith($tar, ".gz") ? '--gunzip' : '';
  $cmd = "tar $options --list --verbose --file $tar 2>&1";
  exec($cmd, $result, $return);
  if ($return === 0) {
    $output = array();
    foreach ($result as $line) {
      array_push($output, tar_decode($line));
    }
    return $output;
  }
  error("Tar_list error for '$cmd'", null, $return, $result);
}

function tar_extract($tar, $file, $level = 0) {
  $options = endsWith($tar, ".gz") ? '--gunzip' : '';
  $cmd = "tar $options --to-stdout --extract --file $tar $file";
  $content = shell_exec($cmd);
  if ($content) {
    return $content;
  }
  error("Tar_extract error for '$cmd'");
}

function tar_stream($tar, $file, $level = 0) {
  if (endsWith($tar, ".gz")) {
    $size = 0;
    $options = '--gunzip';
  } else {
    $size = 0;
    $line = shell_exec("tar --list --verbose --file $tar $file");
    if ($line) {
      list($fname, $mtime, $size, $isdir, $islink) = tar_decode($line);
    }
    $options = '';
  }
  $cmd = "tar $options --to-stdout --extract --file $tar $file 2>&1";
  $mtype = mime_type($file, $tar);
  stream_header($size, $mtype, basename($file));
  passthru($cmd, $return);
  if ($return === 0) {
    exit();
  }
  error("Tar_stream error for '$cmd'", null, $return);
}

function tar_execute($tar, $script) {
  if (endsWith($tar, ".gz")) {
    $options = '--gunzip';
  }
  $tmpdir = tempdir();
  $cmd = "tar $options --extract --directory $tmpdir --file $tar 2>&1";
  exec($cmd, $result, $return);
  if ($return === 0) {
    chdir($tmpdir.'/'.dirname($script));
    include(basename($script));
    exit();
  }
  error("Tar_stream error for '$cmd'", null, $return, $result);
}

function tar_file_exists($list, $path, $level) {
  if ($path == '') {
    return true;
  }
  foreach ($list as $entry) {
    list($fname, $mtime, $fsize, $isdir) = $entry;
    $fname = get_filename($fname, $level);
    if (($fname == $path) || ($fname == $path.'/')) {
      return true;
    }
  }
  return false;
}

function tar_is_dir($list, $path, $level) {
  if ($path == '') {
    return true;
  }
  foreach ($list as $entry) {
    list($fname, $mtime, $fsize, $isdir) = $entry;
    $fname = get_filename($fname, $level);
    if (($fname == $path) || ($fname == $path.'/')) {
      return $isdir;
    }
  }
  return false;
}

function tar_get_index($list, $path, $level) {
  $dir = endsWith($path, '/') ? $path : $path.'/';

  foreach ($list as $entry) {
    list($fname, $mtime, $fsize, $isdir) = $entry;
    $fname = get_filename($fname, $level);
    $index = $dir.'index.html';
    if ($fname == $index) return $index;
    $index = $dir.'index.php';
    if ($fname == $index) return $index;
  }

  return $path;
}


function error($msg, $exception = null, $exit = null, $var = null) {
  $out = '';
  $out .= "Msg: '$msg'\n";
  if ($exception !== null) {
    $out .= "Exc: $exception\n";
  }
  if ($exit !== null) {
    $out .= "Exit: $exit\n";
  }
  if ($var !== null) {
    ob_start();
    var_dump($var);
    $out .= ob_get_clean();
  }
  $size = strlen($out);
  _header("HTTP/1.0 404 Not Found");
  stream_header($size, 'text/plain', 'error.txt');
  print $out;
  exit();
}


$ext_icon_mime = array(
  '/' => array('dir', 'text/plain'),

  'ai' => array('ps', 'application/postscript'),
  'bmp' => array('image2', 'image/bmp'),
  'c' => array('script', 'text/plain'), // text/x-c
  'cc' => array('script', 'text/plain'), // text/x-c
  'cab' => array('tar', 'application/vnd.ms-cab-compressed'),
  'csh' => array('script', 'text/plain'), // application/x-sh
  'css' => array('text', 'text/css'),
  'cxx' => array('script', 'text/x-c'), // text/x-c
  'doc' => array('doc', 'application/msword'),
  'dvi' => array('dvi', 'application/x-dvi'),
  'eps' => array('ps', 'application/postscript'),
  'exe' => array('binary', 'application/x-msdownload'),
  'flv' => array('binary', 'video/x-flv'),
  'gif' => array('image2', 'image/gif'),
  'gz' => array('compressed', 'application/gzip'),
  'h' => array('script', 'text/plain'), // text/x-c
  'htm' => array('text', 'text/html'),
  'html' => array('text', 'text/html'),
  'ico' => array('image2', 'image/x-icon'),
  'jpe' => array('image2', 'image/jpeg'),
  'jpeg' => array('image2', 'image/jpeg'),
  'jpg' => array('image2', 'image/jpeg'),
  'js' => array('script', 'application/javascript'),
  'json' => array('text', 'application/json'),
  'log' => array('text', 'text/plain'),
  'msi' => array('binary', 'application/x-msdownload'),
  'pdf' => array('pdf', 'application/pdf'),
  'php' => array('script', 'text/plain'),
  'png' => array('image2', 'image/png'),
  'ppt' => array('doc', 'application/vnd.ms-powerpoint'),
  'ps' => array('ps', 'application/postscript'),
  'psd' => array('binary', 'application/vnd.adobe.photoshop'),
  'py' => array('script', 'text/x-python'),
  'rar' => array('tar', 'application/x-rar-compressed'),
  'rtf' => array('doc', 'application/rtf'),
  'sh' => array('script', 'text/plain'), // application/x-sh
  'svg' => array('image2', 'image/svg+xml'),
  'svgz' => array('image2', 'image/svg+xml'),
  'swf' => array('binary', 'application/x-shockwave-flash'),
  'tex' => array('tex', 'application/x-tex'),
  'tgz' => array('compressed', 'application/gzip'),
  'tif' => array('image2', 'image/tiff'),
  'tiff' => array('image2', 'image/tiff'),
  'txt' => array('text', 'text/plain'),
  'xls' => array('doc', 'application/vnd.ms-excel'),
  'xml' => array('text', 'application/xml'),
  'zip' => array('tar', 'application/zip'),

  // ours
  'stdout' => array('text', 'text/plain'),
  'stderr' => array('text', 'text/plain'),
  'pickle' => array('script', 'text/plain'),
  'root' => array('binary', 'application/octet-stream'),
);

// https://xxx.web.cern.ch/icons/ PNG
// /icons/unknown.png
// text, image2, tar, ps, pdf, tex, script, dir, dvi, doc, compressed, binary, unknown
function get_icon($fname) {
  global $ext_icon_mime;
  $ext = substr(strrchr($fname, '.'), 1);
  if ($ext && array_key_exists($ext, $ext_icon_mime)) {
    $url = $ext_icon_mime[$ext][0];
    return "/icons/$url.png";
  }

  return "/icons/unknown.png";
}

function mime_type($file_path, $tar = false) {
  global $ext_icon_mime;

  // pre-defined mime types
  $ext = substr(strrchr($file_path, '.'), 1);
  if ($ext && array_key_exists($ext, $ext_icon_mime)) {
    return $ext_icon_mime[$ext][1];
  }

  // mime type is not set, get from server settings
  if ($tar) {
    $line = shell_exec("tar --to-stdout --extract --file $tar $file | file --brief --mime -");
    if ($line) {
      $entry = preg_split('/\;/', $line);
      return $entry[0];
    }
  } else if (function_exists('mime_content_type')) {
    return mime_content_type($file_path);
  } else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
    return $mtype;
  }

  return "application/octet-stream";
}

function sort_by_name($a, $b) {
  return strcasecmp($a[0], $b[0]);
}

function list_html($list, $prefix, $rpath, $vpath = '', $tar = false, $level = 0) {
  global $debug;

  // sorted by name (ignore case) [0]
  usort($list, sort_by_name);

  $maxlen = 30;
  foreach ($list as $entry) {
    $maxlen = max(strlen($entry[0]), $maxlen);
  }

  $readme = '';
  $prefixed_path = $rpath == './' ? "$prefix/" : "$prefix/$rpath$vpath";
  if ($debug) {
    $n = count($list);
    print "list_html(list: $n; prefix: $prefix; rpath: $rpath; vpath: $vpath; tar: $tar; level: $level)\n";
  }

  $vpath = preg_replace('/^tarball_logs(\/)?/', '', $vpath);

  $out = '';

  $out .= "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2 Final//EN\">\n";
  $out .= "<html>\n";
  $out .= " <head>\n";
  $out .= "  <title>ART Index of $prefixed_path</title>\n";
  $out .= " </head>\n";
  $out .= " <body>\n";
  $out .= "   <h1>ART Index of</h1>\n";
  $out .= "   <h2>$prefixed_path</h2>\n";
  $out .= "<pre>\n";

  // need to handle the QueryInfo...
  $out .= "<img src=\"/icons/blank.gif\" alt=\"Icon \"> ";
  // $out .= "<a href=\"?C=N;O=D\">";
  $out .= "Name";
  // $out .= "</a>";
  $out .= str_repeat(' ', max($maxlen - 4 + 1, 0));
  // $out .= "<a href=\"?C=M;O=A\">";
  $out .= "Last modified";
  // $out .= "</a>";
  $out .= str_repeat(' ', 6);
  // $out .= "<a href=\"?C=S;O=A\">";
  $out .= "Size";
  // $out .= "</a>";
  $out .= " \n";
  $out .= "<hr>";
  $out .= "<img src=\"/icons/back.gif\" alt=\"[DIR]\"> <a href=\"..\">Parent Directory</a>";
  $out .= str_repeat(' ', max($maxlen - 16 + 1 + 17 + 1 + 3, 0));
  $out .= "- ";
  $out .= "\n";

  $lpath = get_levelname($list, $level);

  foreach ($list as $entry) {
    list($fname, $mtime, $fsize, $isdir) = $entry;

    // avoid hidden files
    if (startsWith($fname, '.')) {
      continue;
    }

    // move to correct level
    $fname = get_filename($fname, $level);

    // avoid entries that are not part of vpath
    if (!startsWith($fname, $vpath)) {
      continue;
    }

    // remove vpath prefix
    $fname = substr($fname, strlen($vpath));

    // avoid the vpath dir itself
    if ($fname == '') {
      continue;
    }

    // avoid directories deeper than 1 level
    $parts = explode('/', $fname);
    if ((count($parts) > 1) && ($parts[1] != '')) {
      continue;
    }

    // Add README.html as footer to the page
    if ($fname == 'README.html') {
      if ($debug) print "Reading README.html\n";
      if ($tar) {
        $readme = tar_extract($tar, "$lpath$vpath$fname", $level);
      } else {
        $file_path = "$rpath$vpath/$fname";
        $readme = file_get_contents($file_path);
      }

      if ($readme) {
        continue;
      }
      $readme = '';
    }

    // show file or directory
    $icon = get_icon($isdir ? './' : $fname);
    $fdesc = "TBD fdesc";
    $out .= "<img src=\"$icon\" alt=\"[   ]\"> ";
    $out .= "<a href=\"$fname\">$fname</a> ";
    $out .= str_repeat(' ', max($maxlen - strlen($fname), 0));
    $out .= date('d-M-Y H:i', $mtime);
    $size = formatBytes($fsize);
    $out .= str_repeat(' ', max(5 - strlen($size) + 1, 0));
    $out .= "$size ";
    // $out .= " $fdesc";
    $out .= "\n";
  }

  $out .= "<hr>\n";
  $out .= "</pre>\n";
  $out .= "$readme\n";
  $out .= "  </body>\n";
  $out .= "</html>\n";

  $size = strlen($out);
  stream_header($size, 'text/html', 'dirlist.html');
  print $out;
  exit();
}


function stream_header($size, $mtype, $fname) {
  // set headers
  _header("Content-Type: $mtype");
  if ($size > 0) {
    _header("Content-Length: $size");
  }
  if ($mtype === "application/octet-stream") {
      _header("Content-Disposition: attachment; filename=\"$fname\"");
      _header("Content-Transfer-Encoding: binary");
  }
}

function stream_content($file_path, $fname) {
  if ($file_path === false) {
    error("File not found: $fname\n");
  }

  // file size in bytes
  $fsize = filesize($file_path);
  $mtype = mime_type($file_path);

  stream_header($fsize, $mtype, $fname);

  // @readfile($file_path);
  $file = @fopen($file_path,"rb");
  if ($file) {
    while(!feof($file)) {
      print(fread($file, 1024*8));
      flush();
      if (connection_status()!=0) {
        @fclose($file);
        die();
      }
    }
    @fclose($file);
  }

  exit();
}


function get_paths($path) {
  $path = str_replace("//", "/", $path);
  $path_array = preg_split('/\//', $path);

  $rpath = '';
  $vpath = '';
  foreach($path_array as $element) {
    if (($vpath == '') && file_exists($rpath.$element)) {
      $rpath .= $element;
      if (is_dir($rpath) && !endsWith($rpath, '/')) {
        $rpath .= '/';
      }
    } else {
      if ($vpath != '') {
        $vpath .= '/';
      }
      $vpath .= $element;
    }
  }
  return array($rpath, $vpath);
}

function get_tar($rpath, $vpath) {
  foreach (glob("$rpath*.tar") as $tar) {
    if (startsWith($vpath, 'tarball_logs')) {
      // looking for .log.tar
      if (endsWith($tar, '.log.tar')) {
        return $tar;
      }
    } else {
      // looking for .tar
      if (endsWith($tar, '.tar') && !endsWith($tar, '.log.tar')) {
        return $tar;
      }
    }
  }
  return false;
}

// Main Script
date_default_timezone_set('Europe/Paris');

// get path from commandline or from cgi server info
if (array_key_exists('REQUEST_URI', $_SERVER)) {
  $debug = false;

  // deduce prefix
  $script_name = $_SERVER['SCRIPT_NAME'];
  $prefix = dirname($script_name);
  foreach (explode('/', $script_to_dir) as $dir) {
    $prefix = $dir == '..' ? dirname($prefix) : $prefix . '/' . $dir;
  }

  $request_uri = $_SERVER['REQUEST_URI'];
  $path = preg_split('/\?/', $request_uri, 2);
  $path = $path[0];
  if (substr($path, 0, strlen($prefix)) == $prefix) {
    $path = substr($path, strlen($prefix) + 1);
  }

  // go to correct directory
  chdir($script_to_dir);

  // if we need to debug
  $query_string = $_SERVER['QUERY_STRING'];
  parse_str($query_string, $entries);
  if (array_key_exists('debug', $entries)) {
    header("content-type: text/plain");
    print "request_uri:   $request_uri\n";
    print "script_name:   $script_name\n";
    print "path:          $path\n";
    print "prefix:        $prefix\n";
    print "script_to_dir: $script_to_dir\n";
    var_dump($_SERVER);
    exit();
  }
} else {
  $debug = true;
  $argc = $_SERVER['argc'];
  $argv = $_SERVER['argv'];
  if ($argc <= 1) {
    print "Usage:\n";
    print "  command: tar2fs.php <path>\n";
    print "  server:  tar2fs.php/<path>\n";
    exit(1);
  }
  $path = $argv[1];
  $prefix = '';
}
if ($path == '') {
  $path = '.';
}

if ($debug) print " path: $path\n";

// split path in real path (exists on filesystem) and virtual path (may exist in tarfile)
list($rpath, $vpath) = get_paths($path);
if ($debug) {
  print "rpath: $rpath\n";
  print "vpath: $vpath\n";
}

if (!file_exists($rpath)) {
  error("File not found", $path);
}

$rpath = get_index($rpath);

// if real file, stream its content
if (!is_dir($rpath)) {
  if (endsWith($rpath, '.php')) {
    include($rpath);
    exit(0);
  } else {
    stream_content($rpath, basename($rpath));
  }
}

// if real directory, list its content
if ($vpath == '') {
  $list = dir_list($rpath);
  list_html($list, $prefix, $rpath);
}

// get the correct tar file...
$tar = get_tar($rpath, $vpath);
if ($debug) print "tar:   $tar\n";
if (!$tar) {
  error("File not found", $path);
}

// get the content of the tar file
$list = tar_list($tar);
//if ($debug) var_dump($list);

// check that the virtual file or directory exists
$tpath = preg_replace('/^tarball_logs(\/)?/', '', $vpath);
$level = $tpath == $vpath ? 0 : 1;
if ($debug) {
  print "tpath: '$tpath'\n";
  print "level: $level\n";
}

if (!tar_file_exists($list, $tpath, $level)) {
  error("File not found", $path);
}

$tpath = tar_get_index($list, $tpath, $level);

// list the virtual directory or extract the file
if (tar_is_dir($list, $tpath, $level)) {
  $vpath = ($vpath == '') || endsWith($vpath, '/') ? $vpath : $vpath . '/';
  if ($debug) print "vpath: '$vpath'\n";
  list_html($list, $prefix, $rpath, $vpath, $tar, $level);
} else {
  if ($debug) print "tpath: '$tpath'\n";
  if (endsWith($tpath, '.php')) {
    tar_execute($tar, $tpath);
  }
  $lpath = get_levelname($list, $level);
  if ($debug) print "lpath: '$lpath'\n";
  tar_stream($tar, "$lpath$tpath", $level);
}

?>
